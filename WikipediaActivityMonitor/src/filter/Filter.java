package filter;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author V. Galtier
 * 
 *         Read Wikimedia change events from a Kafka topic, filter out the ones
 *         that concern the Wikipedia project, and publish for each of these the
 *         locale of the project to a different Kafka topic.
 */
public class Filter {

	/*
	 * List of Kafka bootstrap servers. Example: localhost:9092,another.host:9092
	 * 
	 * @see:
	 * https://jaceklaskowski.gitbooks.io/apache-kafka/content/kafka-properties-
	 * bootstrap-servers.html
	 */
	private String bootstrapServers;
	/*
	 * Name of the source Kafka topic
	 */
	private String wikimediaChangesTopicName;
	/*
	 * Name of the destination Kafka topic
	 */
	private String localeTopicName;

	public static void main(String[] arg) {
		new Filter(arg[0], arg[1], arg[2]);
	}

	Filter(String bootstrapServers, String wikimediaChangesTopicName, String localeTopicName) {
		this.bootstrapServers = bootstrapServers;
		this.wikimediaChangesTopicName = wikimediaChangesTopicName;
		this.localeTopicName = localeTopicName;

		KafkaConsumer<Void, String> consumer = new KafkaConsumer<Void, String>(
				configureKafkaConsumer(bootstrapServers));
		// read from any partition of the topic
		consumer.subscribe(Collections.singletonList(wikimediaChangesTopicName));

		KafkaProducer<Void, String> producer = new KafkaProducer<Void, String>(
				configureKafkaProducer(bootstrapServers));

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// read events
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Void, String> record : records) {
					// parse Json event
					Gson gson = new Gson();
					JsonObject jsonObject = gson.fromJson(record.value(), JsonObject.class);
					JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
					String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
					// filter
					if (domain.contains("wikipedia.org")) {
						// keep locale only
						String locale = domain.split("\\.")[0];
						// publish to output topic
						producer.send(new ProducerRecord<>(localeTopicName, null, locale));
					}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
			producer.close();
		}

	}

	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}

	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_filters");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
}